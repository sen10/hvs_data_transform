# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 14:40:43 2022

@author: SchwaerzelM
"""
import csv
from termcolor import colored
# import os
#os.chdir(os.path.dirname("C:/Users/Public/Documents/"))

def select_data_ST1(data_path=r"L:/4 dom/1 air/2 immis/0 gest/TaskServer/Inputs/HVS_ST1_test.dat"):
    """
    

    Returns
    -------
    list_data : TYPE
        DESCRIPTION.

    """
    # input 
    start_date = "01.01.22"
    start_str = int(start_date[6:8]+start_date[3:5]+start_date[0:2])
    limit_volume = 1400
    
    list_data = []
    with open(data_path) as f:
        reader = csv.reader(f, delimiter='\t')
        #lines = f.readlines()
        for i in reader:
            if i[0] == 'données filtres':
                if int(i[1][6:8]+i[1][3:5]+i[1][0:2]) >= start_str and float(i[3][2:])>limit_volume:
                    list_data.append(i)
    return list_data 

def write_format_ST1(data, path_to_write="L:/4 dom/1 air/2 immis/0 gest/TaskServer/Inputs/HVS_ST1_newformat.csv"):
    # input
    n_rows = 12
    with open(path_to_write,'w', newline='') as f:
        write = csv.writer(f, delimiter=";")       
        for r in range(n_rows):   
            row = []
            for ix, i in enumerate(data):
                if r==1:
                    row.append(i[r][0:8])
                    row.append("")
                else:
                    row.append(i[r])
                    row.append("")
            write.writerow(row)
    return 1

def select_data_ST3(data_path=r"L:/4 dom/1 air/2 immis/0 gest/TaskServer/Inputs/HVS_ST3_test.dat"):
    """
    Functio
    -------
    Reads HVS data from data_path and separates parameters and values and 
    returns two lists

    Returns
    -------
    p : TYPE str list 
        DESCRIPTION. list of parameters
    v : TYPE str list
        DESCRIPTION. list of values

    """
    #input
    start_date = "Sa 18.12.21"
    start_str = int(start_date[9:11]+start_date[6:8]+start_date[3:5])
    # filter out dates wich would lead to mistakes
    bug_filter_date = ["Ve 20.05.22"] 

    
    with open(data_path) as f:
        lines = f.readlines()
    # len_data = len(lines)    
    text_search = "DEFAULT"
    p = []
    v = []
    for ixx,i in enumerate(lines):
        if i[0:7] == text_search:
            ix = ixx - 2
            if lines[ix][0:2] != "Nr" and lines[ix+4][19:26]!='   0.00' and lines[ix][:11] not in bug_filter_date:
                d = int(lines[ix][9:11]+lines[ix][6:8]+lines[ix][3:5])
                if d>=start_str:
                    for i_line in range(16):
                        p_only = True
                        ix_l = ix+i_line
                        for ipx,ip in enumerate(lines[ix_l]):
                            if ip == ":":
                                p_only = False
                                ip_n = ipx
                                break
                        if p_only:
                            pp = lines[ix_l].strip()
                            vv = ""
                        else:
                            pp = lines[ix_l][0:ip_n].strip()
                            vv = lines[ix_l][ip_n+1:].strip()
                        p = p + [pp]
                        v = v + [vv]
                    p = p + [""]
                    v = v + [""]
 
    return p,v

def write_format_ST3(p, v, path_to_write):
    ''' 
    cette fonction crée un csv qui peut être directement copié dans excel à
    partir d'une liste de paramètres et de valeurs
    
    '''
    # inputs
    n_verti = int(68)
       
    # init len
    len_p = len(p)
    n_c = int(len_p/n_verti)
    n_rest = len_p - n_c*n_verti
    
    with open(path_to_write,'w', newline='') as f:
          write = csv.writer(f, delimiter=";")   
          
          for l in range(n_verti):
              row = []
              for i in range(n_c):
                  ix = l + i*n_verti
                  row_i = [p[ix],v[ix],"","","","","",""]
                  row = row+row_i
              if n_rest > l:
                  ix = int(l+n_c*n_verti)
                  row_i = [p[ix],v[ix],"","","","","",""]
                  row = row+row_i
                  
              write.writerow(row)
              # print(row)
    return 1  
              
def main(
        data_path_ST1=r"L:/4 dom/1 air/2 immis/0 gest/TaskServer/Inputs/HVS_ST1.dat",
        saving_path_ST1="L:/4 dom/1 air/2 immis/0 gest/TaskServer/Inputs/HVS_ST1_newformat.csv",
        data_path_ST2=r"L:/4 dom/1 air/2 immis/0 gest/TaskServer/Inputs/HVS_ST2_test.dat",
        saving_path_ST2="L:/4 dom/1 air/2 immis/0 gest/TaskServer/Inputs/HVS_ST2_newformat.csv",
        data_path_ST3=r"L:/4 dom/1 air/2 immis/0 gest/TaskServer/Inputs/HVS_ST3.dat",
        saving_path_ST3="L:/4 dom/1 air/2 immis/0 gest/TaskServer/Inputs/HVS_ST3_newformat.csv"
        ):

    # station 1
    v = select_data_ST1(data_path_ST1)
    write_format_ST1(v,saving_path_ST1)
    print(colored("Wrote station 1 HVS data from","green"), data_path_ST1, colored("to","green"),saving_path_ST1)

    # station 1
    # v = select_data_ST2(data_path_ST2)
    # write_format_ST2(v,saving_path_ST2)
    # print(colored("Wrote station 2 HVS data from","green"), data_path_ST2, colored("to","green"),saving_path_ST2)
    
    # station 3
    p, v = select_data_ST3(data_path_ST3)
    write_format_ST3(p,v,saving_path_ST3)
    print(colored("Wrote station 3 HVS data from","green"), data_path_ST3, colored("to","green"),saving_path_ST3)

        
if __name__ == "__main__":
    m = main()

        
