# -*- coding: utf-8 -*-
"""
Created on Mon Jul  4 11:33:43 2022

@author: SchwaerzelM
"""

import tkinter as tk
import HVS_data_converter as dc

root= tk.Tk()

canvas1 = tk.Canvas(root, width = 300, height = 300)
canvas1.pack()

dc.main()

# include click button
# def hello ():  
#     label1 = tk.Label(root, text= 'New files ready!', fg='blue', font=('helvetica', 16, 'bold'))
#     dc.main()
#     canvas1.create_window(150, 150, window=label1)
    
# button1 = tk.Button(text='Click To Convert', command=hello, bg='red',fg='white')
# canvas1.create_window(150, 150, window=button1)

# root.mainloop()